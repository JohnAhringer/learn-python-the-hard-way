def cheese_and_crackers(cheese_count, boxes_of_crackers):
	print "You have %d cheeses!" % cheese_count
	print "You have %d boxes of crackers!" % boxes_of_crackers
	print "Man that's enough for a party!"
	print "Get a blanket.\n"
	
def foo_bar(people, not_people, fish, zombies):
	print "There are %r two legged uninfected people" % people
	print "We have %r other things that aren't people" % not_people
	print "This function doesn't really do much."
	return fish + zombies

print "We can just give the function numbers directly:"
cheese_and_crackers(20,30)

print "OR, we can use variables from our script:"
amount_of_cheese = 10
amount_of_crackers = 50

cheese_and_crackers(amount_of_cheese, amount_of_crackers)

print "We can even do math inside too:"
cheese_and_crackers(10 + 20, 5 + 6)

print "And we can combine the two, variables and math:"
cheese_and_crackers(amount_of_cheese + 100, amount_of_crackers + 75)

fish_and_zombies = foo_bar(15, 27, 38, 12)
print fish_and_zombies
fish_and_zombies = foo_bar(None, None, 3, 2)
print fish_and_zombies
fish_and_zombies = foo_bar(12, 55, 55, 74)
print fish_and_zombies


