tabby_cat = "\tI'm tabbed in."
persian_cat = "I'm split\non a line."
backslash_cat = "I'm \\ a \\ cat."

fat_cat = '''
I'll do a list:
\t* Cat food
\t* Fishies
\t* Catnip\n\t* Grass
'''

print tabby_cat
print persian_cat
print backslash_cat
print fat_cat


str1 = "Here is \"example\" of escaping"
str2 = 'This is \'another\' example "of" using stuff'

print "%r" % str1
print "%r" % str2

print "%s" % str1
print "%s" % str2

print "\v"
