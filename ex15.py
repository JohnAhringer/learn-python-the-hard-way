#imports argv to use cli arguments
from sys import argv

#sets the variables script and filename to the cli arguments passed in order
script, filename = argv

#calls the open function with the name of the file, saves it to the txt object
txt = open(filename)

#prints the name of the file
print "Here's your file %r:" % filename

#prints the txt object with the command read, which just prints the content of the file. then closes file
print txt.next()
txt.close()

#printing to the scree
print "Type the filename again:"

#use raw_input to ask the user to type the file in again, filename saved as file_again var
file_again = raw_input("> ")

#txt_again object created by opening the file with the input filename from the user
txt_again = open(file_again)

#calls the read function on the txt_again object to print the contents of the message again. then closes file
print txt_again.read()
txt_again.close()